<?php
    require_once 'php/inc/do_connect.php';

    $user = null;
    
    if (check_auth()) {
        if (!isset($_SESSION['user_id'])){
            $stmt = pdo()->prepare("SELECT * FROM `users` WHERE `User_ID` = :id");
            $stmt->execute(['User_ID' => $_SESSION['user_id']]);
            $user = $stmt->fetch(PDO::FETCH_ASSOC);
        }
        else {
            header('Location: php/actual_orders.php');
        }
    }
?>

<!DOCTYPE html>
<HTML>

<HEAD>
    <link rel="icon" href="images/logotypes/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="css/main.css">
    <meta charset="utf-8">
    <title>Вход в "Tortotoro - для сотрудников"</title>
</HEAD>

<BODY>
    <HEADER>
        <div class="logo_with_descrptn">
            <img id="logotype" src="images/logotypes/favicon.ico" />
            <p class="regular_text--titles"><span id="description">Tortotoro</span></p> <p class="regular_text">для сотрудников</p>
        </div>
        <div class="advice_in_header">
            <p class="regular_text--advices">Не имеете аккаунта? Обратитесь к администратору</p>
        </div>
    </HEADER>
    <MAIN>
        <?php if(isset($_SESSION['user_id'])) {
            header("Location: http://".$_SERVER['HTTP_HOST']."/pm_05_train_practice/php/actual_orders.php");
        ?>
        <?php } else { ?> 
            <div class="log_in">
                <form id="logIN_form" action="php/inc/do_logIN.php" method="post">
                    <label class="regular_text--titles">Войти в систему</label>
                    <div class="logIN_form__password_and_login">
                        <div class="login-input">
                            <label for="user_login">Логин <span id="for_needed_inputs--red_highlight">*</span></label>
                            <input class="regular_text regular_text--inputs" required type="text" id="user_login" name="user_login"
                                placeholder="Введите свой логин (выдаётся администратором)">
                        </div>
                        <div class="password-input">
                            <label for="user_password">Пароль <span id="for_needed_inputs--red_highlight">*</span></label>
                            <input class="regular_text regular_text--inputs" required type="password" id="user_password" name="user_password" placeholder="Введите свой пароль">
                        </div>
                    </div>
                    <div class="submit-and-advice">
                        <button class="regular_text--titles sumbit">Войти</button>
                    </div>
                    <p class="regular_text--advices">Забыли пароль? Обратитесь к администратору</p>
                </form>
            </div>
        <?php } ?>
    </MAIN>
</BODY>

</HTML>