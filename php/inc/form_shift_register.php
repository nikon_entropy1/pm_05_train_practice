<?php
require __DIR__.'/do_connect.php';

$user = null;

if (check_auth()) {

    $stmt = pdo()->prepare("SELECT * FROM `users` WHERE `User_ID` = :id");
    $stmt->execute(['id' => $_SESSION['user_id']]);
    $user = $stmt->fetch(PDO::FETCH_ASSOC);
}
?>
<?php if(isset($_SESSION['user_id']) && $_SESSION['user_role'] == '1') {?>
<!DOCTYPE html>
<HTML>

<HEAD>
    <link rel="icon" href="../../images/logotypes/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../../css/main.css">
    <meta charset="utf-8">
    <title>Создание смены</title>
</HEAD>

<BODY>
    <HEADER>
        <div class="logo_with_descrptn">
            <img id="logotype" src="../../images/logotypes/favicon.ico" />
            <p class="regular_text--titles"><span id="description">Tortotoro</span></p>
            <p class="regular_text">для сотрудников</p>
        </div>
        <div class="advice_in_header">
            <?php
                if ($_SESSION['user_role'] == '1'){
                    echo '<p class="regular_text--advices">Администратору - бдительность и исполнительность</p>';
                }
                else {
                    echo '<p class="regular_text--advices">Как вы тут оказались?</p>';
                }
            ?>
        </div>
    </HEADER>
    <MAIN>
        <div class="reg_in">
            <form id="register_form" action="do__add_new_shift.php" method="post">
                <label class="regular_text--titles">Добавить новую смену</label>
                <div class="register_form__password_and_login">
                    <div class="shift_date-input">
                        <label for="shift_date">Дата смены <span id="for_needed_inputs--red_highlight">*</span></label>
                        <input class="regular_text regular_text--inputs" min="2023-05-01" max="2023-12-31" required type="date" id="shift_date"
                            name="shift_date" placeholder="Введите дату новой смены">
                    </div>
                    <div class="shift_start_time-input">
                        <label for="shift_start_time">Время начала смены <span id="for_needed_inputs--red_highlight">*</span></label>
                        <input class="regular_text regular_text--inputs" required type="time" id="shift_start_time"
                            name="shift_start_time" placeholder="Введите время начала смены">
                    </div>
                    <div class="shift_end_time-input">
                        <label for="shift_end_time">Время окончания смены <span id="for_needed_inputs--red_highlight">*</span></label>
                        <input class="regular_text regular_text--inputs" required type="time" id="shift_end_time"
                            name="shift_end_time" placeholder="Введите время окончания смены">
                    </div>
                    <div class="shift_status-input">
                        <label for="shift_status">Состояние смены <span id="for_needed_inputs--red_highlight">*</span></label>
                        <input class="regular_text regular_text--inputs" required type="number" min="1" max="3" id="shift_status"
                            name="shift_status" placeholder="Введите номер состояния">
                        <p class="regular_text--advices">Назначая состояния, помни: 
                            <ol id="shift_states">
                                <li class="regular_text--advices">1 - Открыта (Назначается администратором перед непосредственным началом смены);</li>
                                <li class="regular_text--advices">2 - Закрыта (Назначается администратором в том случае, если закончился рабочий день/смена);</li>
                                <li class="regular_text--advices">3 - Добавлена (Назначается администратором заранее, чтобы распланировать смены).</li>
                            </ol>
                        </p>
                    </div>
                </div>
                <div class="submit-and-advice">
                    <button class="regular_text--titles sumbit">Добавить</button>
                </div>
            </form>
        </div>
    </MAIN>
</BODY>

</HTML>
<?php } else {
    echo '<link rel="stylesheet" href="../../css/main.css">';
    echo
        '<MAIN id="error_page--auth" class="error_page">' .
            '<div class="error_notice">
                        <h2 class="regular_text--titles">У вас нет доступа к этой странице</h2>' .
            '<h3 class="regular_text--advices">(Вы будете перенаправлены на страницу для авторизации через 3 секунды)</h3>' .
            '</div>' .
        '</MAIN>';
    header('refresh: 3, url=../../index.php');
    die;
}?>