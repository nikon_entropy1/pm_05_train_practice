<?php

require_once __DIR__ . '/do_connect.php';

$stmt = pdo()->prepare("SELECT * FROM `users` WHERE `User_Username` = :user_login");
$stmt->execute(['user_login' => $_POST['user_login']]);
if ($stmt->rowCount() > 0) {
    header("Location: {$_SERVER['HTTP_REFERER']}");
    die;
}

if (isset($_SESSION['user_id']) && isset($_SESSION['user_role'])){

    if ($_SESSION['user_role'] == '1' || $_SESSION['user_role'] == '2'){
        $stmt = pdo()->prepare(
            "INSERT INTO `orders`
                (`Order_Status`,
                `Order_IsPaid`) 
                VALUES 
                (:order_status, 
                :order_isPaid);"
        );
    
        $stmt->execute([
            'order_status' => $_POST['order_status__select'],
            'order_isPaid' => $_POST['order_IsPaid__select'],
        ]);
    
        echo '<link rel="stylesheet" href="../../css/main.css">';
        echo
            '<MAIN id="error_page--auth" class="error_page">' .
            '<div class="error_notice">
                                <h2 class="regular_text--titles">Заказ был успешно добавлен</h2>' .
            '<h3 class="regular_text--advices">(Вы будете перенаправлены на предыдущую страницу через 3 секунды)</h3>' .
            '</div>' .
            '</MAIN>';
        header('refresh: 3, url=../actual_orders.php');
        die;
    }

    else {
        echo '<link rel="stylesheet" href="../../css/main.css">';
        echo
            '<MAIN id="error_page--auth" class="error_page">' .
            '<div class="error_notice">
                            <h2 class="regular_text--titles">У вас нет доступа к этой странице</h2>' .
            '<h3 class="regular_text--advices">(Вы будете перенаправлены на страницу для авторизации через 3 секунды)</h3>' .
            '</div>' .
            '</MAIN>';
            header('refresh: 3, url=../../index.php');
        die;
    }
}



