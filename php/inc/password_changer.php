<?php
    if (isset($_SESSION['user_role']) 
    && isset($_POST['old_pass']) 
    && isset($_POST['new_pass']) 
    && isset($_POST['new_pass_check'])){
        require_once __DIR__.'/do_connect.php';

        $stmt = pdo()->prepare(
            "SELECT `User_Password`
            FROM 
                `users` 
            WHERE 
                `User_ID` = :user_login");
        $stmt->execute(['user_login' => $_SESSION['user_id']]);
        $user_password = $stmt->fetch(PDO::FETCH_ASSOC);
        
        $old_password = password_hash($_POST['old_pass'], PASSWORD_ARGON2ID);

        if (password_verify($_POST['old_pass'], $user_password['User_Password'])) {
            
            $new_password = strip_tags($_POST['new_pass']);
            $new_password_check = strip_tags($_POST['new_pass_check']);

            if ($new_password == $new_password_check){
                $new_password__hashed = password_hash($new_password, PASSWORD_ARGON2ID);

                $stmt = pdo()->prepare(
                    'UPDATE 
                        `users` 
                    SET 
                        `User_Password` = :user_password 
                    WHERE 
                        `User_ID` = :user_login');
                $stmt->execute([
                    'user_login' => $_SESSION['user_id'],
                    'user_password' => $new_password__hashed,
                ]);

                echo '<link rel="stylesheet" href="../../css/main.css">';
                echo
            '<MAIN id="error_page--auth" class="error_page">' .
                '<div class="error_notice">
                    <h2 class="regular_text--titles">Пароль был успешно изменён</h2>' .
                    '<h3 class="regular_text--advices">(Вы будете перенаправлены на предыдущую страницу через 3 секунды)</h3>' .
                '</div>' .
            '</MAIN>';
            header("Location: {$_SERVER['HTTP_REFERER']}");
            die;
            }

            else
            {
                echo '<link rel="stylesheet" href="../../css/main.css">';
                echo
            '<MAIN id="error_page--auth" class="error_page">' .
                '<div class="error_notice">
                    <h2 class="regular_text--titles">Пароли не совпадают</h2>' .
                    '<h3 class="regular_text--advices">(Вы будете перенаправлены на предыдущую страницу через 3 секунды)</h3>' .
                '</div>' .
            '</MAIN>';
            header("Location: {$_SERVER['HTTP_REFERER']}");
            die;
            }
        }

        else {
            echo '<link rel="stylesheet" href="../../css/main.css">';
            echo
            '<MAIN id="error_page--auth" class="error_page">' .
                '<div class="error_notice">
                    <h2 class="regular_text--titles">Вы ввели неправильный пароль</h2>' .
                    '<h3 class="regular_text--advices">(Вы будете перенаправлены на предыдущую страницу через 3 секунды)</h3>' .
                '</div>' .
            '</MAIN>';
            header("Location: {$_SERVER['HTTP_REFERER']}");
            die;
        }
    }
    
    else 
    {
        echo '<link rel="stylesheet" href="../../css/main.css">';
        echo
        '<MAIN id="error_page--auth" class="error_page">' .
            '<div class="error_notice">
                <h2 class="regular_text--titles">Вы не авторизованы или не ввели необходимые данные</h2>' .
                '<h3 class="regular_text--advices">(Вы будете перенаправлены на предыдущую страницу через 3 секунды)</h3>' .
            '</div>' .
        '</MAIN>';
        header('refresh: 3, url=../../index.php');
        die;
    }
?>