<?php

session_start();
function pdo(): PDO
{
    static $pdo;

    if (!$pdo) {
        if (file_exists(__DIR__ . '/config.php')) {
            $config = include __DIR__.'/config.php';
        } else {
            $msg = 'Создайте и настройте config.php на основе config.sample.php';
            trigger_error($msg, E_USER_ERROR);
        }
        
        $dsn = 'mysql:dbname='.$config['db_name'].';host='.$config['db_host'];
        $pdo = new PDO($dsn, $config['db_user'], $config['db_pass']);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    return $pdo;
}

function check_auth(): bool
{
    return(isset($_SESSION['user_id'])) ? true : false;
}

function error__del_a_row(){
            echo
            '<MAIN id="error_page--auth" class="error_page">' .
                '<div class="error_notice">
                    <h2 class="regular_text--titles">Строка не была удалена</h2>' .
                    '<h3 class="regular_text--advices">(Вы будете перенаправлены на предыдущую страницу через 3 секунды)</h3>' .
                '</div>' .
            '</MAIN>';
}