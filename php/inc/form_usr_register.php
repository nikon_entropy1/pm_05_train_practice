<?php
require __DIR__.'/do_connect.php';

$user = null;

if (check_auth()) {

    $stmt = pdo()->prepare("SELECT * FROM `users` WHERE `User_ID` = :id");
    $stmt->execute(['id' => $_SESSION['user_id']]);
    $user = $stmt->fetch(PDO::FETCH_ASSOC);
}
?>
<?php if(isset($_SESSION['user_id']) && $_SESSION['user_role'] == '1'){?>
<!DOCTYPE html>
<HTML>

<HEAD>
    <link rel="icon" href="../../images/logotypes/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../../css/main.css">
    <meta charset="utf-8">
    <title>Регистрация сотрудника</title>
</HEAD>

<BODY>
    <HEADER>
        <div class="logo_with_descrptn">
            <img id="logotype" src="../../images/logotypes/favicon.ico" />
            <p class="regular_text--titles"><span id="description">Tortotoro</span></p>
            <p class="regular_text">для сотрудников</p>
        </div>
        <div class="advice_in_header">
            <?php
                if ($_SESSION['user_role'] == '1'){
                    echo '<p class="regular_text--advices">Администратору - внимательность</p>';
                }
                else {
                    echo '<p class="regular_text--advices">Как вы тут оказались?</p>';
                }
            ?>
        </div>
    </HEADER>
    <MAIN>
        <div class="reg_in">
            <form id="register_form" action="do_register.php" method="post">
                <label class="regular_text--titles">Зарегистрировать пользователя в системе</label>
                <div class="register_form__password_and_login">
                    <div class="login-input">
                        <label for="user_login">Логин <span id="for_needed_inputs--red_highlight">*</span></label>
                        <input class="regular_text regular_text--inputs" required type="text" id="user_login"
                            name="user_login" placeholder="Введите логин">
                    </div>
                    <div class="password-input">
                        <label for="user_password">Пароль <span id="for_needed_inputs--red_highlight">*</span></label>
                        <input class="regular_text regular_text--inputs" required type="password" id="user_password"
                            name="user_password" placeholder="Введите пароль">
                    </div>
                    <div class="role-input">
                        <label for="user_role">Роль <span id="for_needed_inputs--red_highlight">*</span></label>
                        <input class="regular_text regular_text--inputs" required type="number" min="1" max="3" id="user_role"
                            name="user_role" placeholder="Введите номер роли">
                        <p class="regular_text--advices">Назначая роли, помни: 
                            <ol>
                                <li class="regular_text--advices">1 - Администратор;</li>
                                <li class="regular_text--advices">2 - Официант;</li>
                                <li class="regular_text--advices">3 - Повар.</li>
                            </ol>
                        </p>
                    </div>
                    <div class="firstname-input">
                        <label for="user_firstname">Имя <span id="for_needed_inputs--red_highlight">*</span></label>
                        <input class="regular_text regular_text--inputs" required type="text" id="user_firstname"
                            name="user_firstname" placeholder="Введите имя сотрудника">
                    </div>
                    <div class="lastname-input">
                        <label for="user_lastname">Фамилия <span id="for_needed_inputs--red_highlight">*</span></label>
                        <input class="regular_text regular_text--inputs" required type="text" id="user_lastname"
                            name="user_lastname" placeholder="Введите фамилию сотрудника">
                    </div>
                    <div class="patronimyc-input">
                        <label for="user_patronimyc">Отчество</label>
                        <input class="regular_text regular_text--inputs" type="text" id="user_patronimyc"
                            name="user_patronimyc" placeholder="Введите отчество сотрудника">
                    </div>
                    <div class="birthday-input">
                        <label for="user_birthday">Дата рождения <span id="for_needed_inputs--red_highlight">*</span></label>
                        <input class="regular_text regular_text--inputs" min="1950-01-01" max="2023-12-31" required type="date" id="user_birthday"
                            name="user_birthday" placeholder="Введите дату рождения сотрудника">
                    </div>
                    <div class="gender-input">
                        <label for="user_gender">Пол <span id="for_needed_inputs--red_highlight">*</span></label>
                        <input class="regular_text regular_text--inputs" required type="number" min="0" max="1" id="user_gender"
                            name="user_gender" placeholder="Введите номер пола">
                        <p class="regular_text--advices">Назначая пол, помни: 
                            <ol>
                                <li class="regular_text--advices">0 - Мужчина;</li>
                                <li class="regular_text--advices">1 - Женщина;</li>
                            </ol>
                        </p>
                    </div>
                    <div class="telephone-input">
                        <label for="user_telephone">Номер телефона <span id="for_needed_inputs--red_highlight">*</span></label>
                        <input class="regular_text regular_text--inputs" required type="tel" maxlength="11" id="user_telephone"
                            name="user_telephone" placeholder="Введите номер телефона сотрудника">
                    </div>
                    <div class="email-input">
                        <label for="user_email">Электронная почта</label>
                        <input class="regular_text regular_text--inputs" type="text" id="user_email"
                            name="user_email" placeholder="Введите электронную почту сотрудника">
                    </div>
                </div>
                <div class="submit-and-advice">
                    <button class="regular_text--titles sumbit">Зарегистрировать</button>
                </div>
            </form>
        </div>
    </MAIN>
</BODY>

</HTML>
<?php } else {
    echo '<link rel="stylesheet" href="../../css/main.css">';
    echo
        '<MAIN id="error_page--auth" class="error_page">' .
            '<div class="error_notice">
                        <h2 class="regular_text--titles">У вас нет доступа к этой странице</h2>' .
            '<h3 class="regular_text--advices">(Вы будете перенаправлены на страницу для авторизации через 3 секунды)</h3>' .
            '</div>' .
        '</MAIN>';
    header('refresh: 3, url=../../index.php');
    die;
}?>