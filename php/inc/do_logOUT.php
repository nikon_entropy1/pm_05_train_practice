
<?php

require_once __DIR__.'/do_connect.php';

unset($_SESSION['user_id']);
unset($_SESSION['user_role']);

session_destroy();

header('Location: /pm_05_train_practice/');