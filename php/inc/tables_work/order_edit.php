<?php
    require_once '../do_connect.php';

    if (check_auth()) {

        $stmt = pdo()->prepare("SELECT * FROM `users` WHERE `User_ID` = :id");
        $stmt->execute(['id' => $_SESSION['user_id']]);
        $user = $stmt->fetch(PDO::FETCH_ASSOC);
    }

    if (isset($_SESSION['user_id']) && ($_SESSION['user_role'] >= '1' 
    && $_SESSION['user_role'] <= '3')
    && isset($_POST['row_to_edit__order_id'])){
        
        $row_to_edit__order_id = $_POST['row_to_edit__order_id'];

        $old_order_status = $_POST['row_to_edit__order_old_status'];
        
        if ($_SESSION['user_role'] == '1'){
            
            if (isset($_POST['order_status__select']) && isset($_POST['order_IsPaid__select'])){
                
                $order_new_status = $_POST['order_status__select'];
                $order_new_isPaid = $_POST['order_IsPaid__select'];

                $stmt = pdo()->prepare(
                    'UPDATE 
                        `orders` 
                    SET 
                        `Order_Status` = :order_status,
                        `Order_IsPaid` = :order_IsPaid
                    WHERE 
                        `Order_ID` = :order_id');

                $stmt->execute([
                    'order_id' => $row_to_edit__order_id,
                    'order_status' => $order_new_status,
                    'order_IsPaid' => $order_new_isPaid
                ]);

                echo '<link rel="stylesheet" href="../../../css/main.css">';
                echo
                    '<MAIN id="error_page--auth" class="error_page">' .
                        '<div class="error_notice">
                            <h2 class="regular_text--titles">Заказ был успешно изменён</h2>' .
                            '<h3 class="regular_text--advices">(Вы будете перенаправлены на предыдущую страницу через 3 секунды)</h3>' .
                        '</div>' .
                    '</MAIN>';
                header('refresh: 3, url=../../actual_orders.php');
                die;
            }

            else {
                echo 'Значения не были переданы';
            }
        }


        elseif ($_SESSION['user_role'] == '2'){
            
            if (isset($_POST['order_status__select']) 
            && $old_order_status == 'Готов'
            && isset($_POST['order_IsPaid__select'])){

                $order_new_status = $_POST['order_status__select'];
                $order_new_isPaid = $_POST['order_IsPaid__select'];

                $stmt = pdo()->prepare(
                    'UPDATE 
                        `orders` 
                    SET 
                        `Order_Status` = :order_status,
                        `Order_IsPaid` = :order_IsPaid
                    WHERE 
                        `Order_ID` = :order_id');

                $stmt->execute([
                    'order_id' => $row_to_edit__order_id,
                    'order_status' => $order_new_status,
                    'order_IsPaid' => $order_new_isPaid
                ]);

                echo '<link rel="stylesheet" href="../../../css/main.css">';
                echo
                    '<MAIN id="error_page--auth" class="error_page">' .
                        '<div class="error_notice">
                            <h2 class="regular_text--titles">Заказ был успешно изменён</h2>' .
                            '<h3 class="regular_text--advices">(Вы будете перенаправлены на предыдущую страницу через 3 секунды)</h3>' .
                        '</div>' .
                    '</MAIN>';
                header('refresh: 3, url=../../actual_orders.php');
                die;
            }

            else {
                echo 'Значения не были переданы';
            }
        }
        
        elseif ($_SESSION['user_role'] == '3'){

            if (isset($_POST['order_status__select']) 
            && ($old_order_status == 'Принят'|| $old_order_status == 'Готовится')){

                $order_new_status = $_POST['order_status__select'];

                $stmt = pdo()->prepare(
                    'UPDATE 
                        `orders` 
                    SET 
                        `Order_Status` = :order_status
                    WHERE 
                        `Order_ID` = :order_id');

                $stmt->execute([
                    'order_id' => $row_to_edit__order_id,
                    'order_status' => $order_new_status
                ]);

                echo '<link rel="stylesheet" href="../../../css/main.css">';
                echo
                    '<MAIN id="error_page--auth" class="error_page">' .
                        '<div class="error_notice">
                            <h2 class="regular_text--titles">Заказ был успешно изменён</h2>' .
                            '<h3 class="regular_text--advices">(Вы будете перенаправлены на предыдущую страницу через 3 секунды)</h3>' .
                        '</div>' .
                    '</MAIN>';
                header('refresh: 3, url=../../actual_orders.php');
                die;
            }

            elseif ($_POST['old_order_status'] =! '3'){
                echo 'Вы можете изменять только принятые или готовящиеся заказы';
            }

            else {
                echo 'Значения не были переданы';
            }
        }

    }
    
    else {
            echo 'У вас нет доступа к данной странице';
        }
?>