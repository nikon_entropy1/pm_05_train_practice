<?php
    require_once '../do_connect.php';

    if (check_auth()) {

        $stmt = pdo()->prepare("SELECT * FROM `users` WHERE `User_ID` = :id");
        $stmt->execute(['id' => $_SESSION['user_id']]);
        $user = $stmt->fetch(PDO::FETCH_ASSOC);
    }

    if (isset($_SESSION['user_id']) && ($_SESSION['user_role'] == '1')){
        
        if (isset ($_POST['row_to_edit__shift_id'])
        && isset ($_POST['shift_date'])
        && isset ($_POST['shift_start_time'])
        && isset ($_POST['shift_end_time'])
        && isset ($_POST['shift_status'])){
            
            $row_to_edit__id = $_POST['row_to_edit__shift_id'];
            $row_to_edit__shift_date = $_POST['shift_date'];
            $row_to_edit__shift_start_time = $_POST['shift_start_time'];
            $row_to_edit__shift_end_time = $_POST['shift_end_time'];
            $row_to_edit__shift_status = $_POST['shift_status'];
            
            $stmt = pdo()->prepare(
                'UPDATE 
                    `shifts` 
                SET 
                    `Shift_date` = :shift_date,
                    `Shift_Start__Time` = :shift_start_time,
                    `Shift_End__Time` = :shift_end_time,
                    `Shift_Status` = :shift_status
                WHERE 
                    `Shift_ID` = :shift_id');

            $stmt->execute([
                'shift_id' => $row_to_edit__id,
                'shift_date' => $row_to_edit__shift_date,
                'shift_start_time' => $row_to_edit__shift_start_time,
                'shift_end_time' => $row_to_edit__shift_end_time,
                'shift_status' => $row_to_edit__shift_status
            ]);

            echo '<link rel="stylesheet" href="../../../css/main.css">';
            echo
                '<MAIN id="error_page--auth" class="error_page">' .
                    '<div class="error_notice">
                        <h2 class="regular_text--titles">Смена была успешно изменена</h2>' .
                        '<h3 class="regular_text--advices">(Вы будете перенаправлены на предыдущую страницу через 3 секунды)</h3>' .
                    '</div>' .
                '</MAIN>';
            header('refresh: 3, url=../../actual_orders.php');
            die;
        }

        else {
            echo 'Значения не были переданы';
        }
    }

    else {
        echo 'Вы, наверное, пытаетесь получить доступ без разрешения';
    }
?>