<?php
    require_once '../do_connect.php';

    if (isset($_SESSION['user_id']) && $_SESSION['user_role'] == '1' && $_POST['row_to_delete__employee_id']){

        $row_to_delete = $_POST['row_to_delete__employee_id'];

        $stmt = pdo()->prepare(
            'DELETE 
            
            FROM 
                `users` 
            WHERE 
                `Employee_ID__FK` = :user_id');

        $stmt->execute([
            'user_id' => $row_to_delete,
        ]);
        
        if ($stmt->rowCount() > 0) {
            header("Location: {$_SERVER['HTTP_REFERER']}");
            die;
        }
        
        else {
            echo '<link rel="stylesheet" href="../../../css/main.css">';
            echo
            '<MAIN id="error_page--auth" class="error_page">' .
                '<div class="error_notice">
                    <h2 class="regular_text--titles">Строка не была удалена</h2>' .
                    '<h3 class="regular_text--advices">(Вы будете перенаправлены на предыдущую страницу через 3 секунды)</h3>' .
                '</div>' .
            '</MAIN>';
        }
        
    }
    else {
        echo '<link rel="stylesheet" href="../../../css/main.css">';
        echo
            '<MAIN id="error_page--auth" class="error_page">' .
                '<div class="error_notice">
                            <h2 class="regular_text--titles">У вас нет доступа к этой странице</h2>' .
                '<h3 class="regular_text--advices">(Вы будете перенаправлены на страницу для авторизации через 3 секунды)</h3>' .
                '</div>' .
            '</MAIN>';
        header('refresh: 3, url=../../../index.php');
        die;
    }
?>