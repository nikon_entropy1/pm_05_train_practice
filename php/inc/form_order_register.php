<?php
require_once __DIR__ . '/do_connect.php';

$user = null;

if (check_auth()) {

    $stmt = pdo()->prepare("SELECT * FROM `users` WHERE `User_ID` = :id");
    $stmt->execute(['id' => $_SESSION['user_id']]);
    $user = $stmt->fetch(PDO::FETCH_ASSOC);
}
?>

<?php if(isset($_SESSION['user_id']) && ($_SESSION['user_role'] == '1' || $_SESSION['user_role'] == '2')) {?>
    <!DOCTYPE html>
    <HTML>

    <HEAD>
        <link rel="icon" href="../../images/logotypes/favicon.ico" type="image/x-icon">
        <link rel="stylesheet" href="../../css/main.css">
        <meta charset="utf-8">
        <title>Добавление заказа</title>
    </HEAD>

    <BODY>
        <HEADER>
            <div class="logo_with_descrptn">
                <img id="logotype" src="../../images/logotypes/favicon.ico" />
                <p class="regular_text--titles"><span id="description">Tortotoro</span></p>
                <p class="regular_text">для сотрудников</p>
            </div>
            <div class="advice_in_header">
                <?php
                    if ($_SESSION['user_role'] == '1'){
                        echo '<p class="regular_text--advices">Администратору - не терять бдительность и исполнительность</p>';
                    }
                    else {
                        echo '<p class="regular_text--advices">Будьте внимательны при заполнении полей!</p>';
                    }
                ?>
            </div>
        </HEADER>
        <MAIN>
            <div class="reg_in">
                <form id="register_form" action="do__add_new_order.php" method="post">
                    <label class="regular_text--titles">Добавить новый заказ</label>
                    <div class="register_form__password_and_login">
                        <?php
                            if ($_SESSION['user_role'] == '1'){
                                echo '
                                <div class="order_status-input">
                                    <label for="order_status">Состояние заказа</label>
                                    <input class="regular_text regular_text--inputs" type="number" min="1" max="5" id="order_status"
                                        name="order_status" placeholder="Введите номер состояния">
                                    <p class="regular_text--advices">Назначая состояния, помни:</p>
                                    <ol id="order_states">
                                        <li class="regular_text--advices">1 - Принят (Назначается по умолчанию);</li>
                                        <li class="regular_text--advices">2 - Готовится (Назначается в том случае, если был принят
                                            поваром);</li>
                                        <li class="regular_text--advices">3 - Готов (Назначается поваром по приготовлении).</li>
                                        <li class="regular_text--advices">4 - Оплачен (Назначается официантом после оплаты).</li>
                                        <li class="regular_text--advices">5 - Отменён (Назначается официантом в случае эксцесса).
                                        </li>
                                    </ol>
                                </div>
                                <div class="order_isPaid-input">
                                    <label for="order_isPaid">Состояние оплаты</label>
                                    <input class="regular_text regular_text--inputs" type="number" min="0" max="1" id="order_isPaid"
                                        name="order_isPaid" placeholder="Введите номер состояния">
                                    <p class="regular_text--advices">Назначая состояния, помни:</p>
                                    <ol id="order_isPaid">
                                        <li class="regular_text--advices">0 - Не оплачен;</li>
                                        <li class="regular_text--advices">1 - Оплачен ;</li>
                                    </ol>
                                </div>
                            </div>         
                            <div class="submit-and-advice">
                                <button class="regular_text--titles sumbit">Добавить</button>
                            </div>';
                            }

                            if ($_SESSION['user_role'] == '2'){
                                echo '
                                <div class="order_status-input">
                                    <label for="order_status__select">Состояние заказа</label>
                                    <select class="regular_text regular_text--inputs" name="order_status__select">
                                        <option value="1">Принят</option>
                                        <option value="4">Оплачен</option>
                                        <option value="5">Отменён</option>
                                    </select>
                                    <p class="regular_text--advices">Назначая состояния, помни:</p>
                                    <ol id="order_states">
                                        <li class="regular_text--advices">1 - Принят (Назначается по умолчанию);</li>
                                        <li class="regular_text--advices">4 - Оплачен (Назначается официантом после оплаты).</li>
                                        <li class="regular_text--advices">5 - Отменён (Назначается официантом в случае эксцесса).</li>
                                    </ol>
                                </div>
                                <div class="order_isPaid-input">
                                    <label for="order_IsPaid__select">Cостояние оплаты</label>
                                    <select class="regular_text regular_text--inputs" name="order_IsPaid__select">
                                        <option value="0">Не оплачен</option>
                                        <option value="1">Оплачен</option>
                                    </select>
                                </div>
                            </div>         
                            <div class="submit-and-advice">
                                <button class="regular_text--titles sumbit">Добавить</button>
                            </div>';
                            }

                            if ($_SESSION['user_role'] == '3'){
                                echo '<link rel="stylesheet" href="../../css/main.css">';
                                echo
                                    '<MAIN id="error_page--auth" class="error_page">' .
                                    '<div class="error_notice">
                                                    <h2 class="regular_text--titles">Вы не можете добавлять заказы</h2>' .
                                    '<h3 class="regular_text--advices">(Вы будете перенаправлены на страницу для авторизации через 3 секунды)</h3>' .
                                    '</div>' .
                                    '</MAIN>';
                                header('refresh: 3, url=../../index.php');
                                die;
                            }
                        ?>
                </form>
            </div>
        </MAIN>
    </BODY>

    </HTML>
<?php } else {
    echo '<link rel="stylesheet" href="../../css/main.css">';
    echo
        '<MAIN id="error_page--auth" class="error_page">' .
        '<div class="error_notice">
                        <h2 class="regular_text--titles">У вас нет доступа к этой странице</h2>' .
        '<h3 class="regular_text--advices">(Вы будете перенаправлены на страницу для авторизации через 3 секунды)</h3>' .
        '</div>' .
        '</MAIN>';
    header('refresh: 3, url=../../index.php');
    die;
} ?>