<?php

require_once __DIR__.'/do_connect.php';

$stmt = pdo()->prepare("SELECT * FROM `users` WHERE `User_Username` = :user_login");
$stmt->execute(['user_login' => $_POST['user_login']]);
if ($stmt->rowCount() > 0) {
    header("Location: {$_SERVER['HTTP_REFERER']}");
    die;
}

if ($_SESSION['user_role'] == '1'){
    
    if (is_numeric($_POST['user_gender'])){

        if ($_POST['user_gender'] == '0'){

            $stmt = pdo()->prepare(

                "INSERT INTO `users`
                    (`User_Username`,
                    `User_Password`,
                    `User_Avatar`,
                    `User_Role`) 
                    VALUES 
                    (:user_login, 
                    :user_password,
                    :user_avatar, 
                    :user_role);
            ");

            $stmt->execute([
                'user_login' => strip_tags($_POST['user_login']),
                'user_password' => password_hash($_POST['user_password'], PASSWORD_ARGON2ID),
                'user_avatar' => '1',
                'user_role' => strip_tags($_POST['user_role'])
            ]);
        }

        elseif ($_POST['user_gender'] == '1'){

            $stmt = pdo()->prepare(

                "INSERT INTO `users`
                    (`User_Username`,
                    `User_Password`,
                    `User_Avatar`,
                    `User_Role`) 
                    VALUES 
                    (:user_login, 
                    :user_password,
                    :user_avatar, 
                    :user_role);
            ");

            $stmt->execute([
                'user_login' => strip_tags($_POST['user_login']),
                'user_password' => password_hash($_POST['user_password'], PASSWORD_ARGON2ID),
                'user_avatar' => '2',
                'user_role' => strip_tags($_POST['user_role'])
            ]);
        }

        else {
            echo '<link rel="stylesheet" href="../../css/main.css">';
            echo
            '<MAIN id="error_page--auth" class="error_page">' .
            '<div class="error_notice">
                            <h2 class="regular_text--titles">Получено недопустимое значение</h2>' .
            '<h3 class="regular_text--advices">(Вы будете перенаправлены на предыдущую страницу через 3 секунды)</h3>' .
            '</div>' .
            '</MAIN>';
            header('refresh: 3, url=../../index.php');
            die;
        }
    }

    $stmt = pdo()->prepare(
        "INSERT INTO `personell` 
            (`Employee_Firstname`, 
            `Employee_Lastname`, 
            `Employee_Patronimyc`, 
            `Employee_Birthday`,
            `Employee_Gender`, 
            `Employee_Telephone`, 
            `Employee_Email`)
            VALUES 
                (:user_firstname, 
                :user_lastname, 
                :user_patronimyc, 
                :user_birthday, 
                :user_gender, 
                :user_telephone, 
                :user_email)
    ");

    $stmt->execute([
        'user_firstname' => strip_tags($_POST['user_firstname']),
        'user_lastname' => strip_tags($_POST['user_lastname']),
        'user_patronimyc' => strip_tags($_POST['user_patronimyc']),
        'user_gender' => strip_tags($_POST['user_gender']),
        'user_birthday' => strip_tags($_POST['user_birthday']),
        'user_telephone' => strip_tags($_POST['user_telephone']),
        'user_email' => strip_tags($_POST['user_email']),
    ]);

    header("Location: {$_SERVER['HTTP_REFERER']}");
}

else {
    echo '<link rel="stylesheet" href="../../css/main.css">';
    echo
            '<MAIN id="error_page--auth" class="error_page">' .
            '<div class="error_notice">
                            <h2 class="regular_text--titles">У вас нет доступа к этой странице</h2>' .
            '<h3 class="regular_text--advices">(Вы будете перенаправлены на страницу для авторизации через 3 секунды)</h3>' .
            '</div>' .
            '</MAIN>';
    header('refresh: 3, url=../../index.php');
    die;
}

