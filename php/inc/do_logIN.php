<?php

require_once __DIR__ . '/do_connect.php';

$stmt = pdo()->prepare(
    "SELECT * 
    FROM 
        `users` 
    WHERE 
        `User_Username` = :user_login");
$stmt->execute(['user_login' => $_POST['user_login']]);

if (!$stmt->rowCount()) {
    echo '<link rel="stylesheet" href="../../css/main.css">';
    echo
        '<MAIN id="error_page--auth" class="error_page">'.
            '<div class="error_notice">
                <h2 class="regular_text--titles">Данный пользователь (' . $_POST['user_login'] . ') не зарегистрирован!</h2>' .
            '<h3 class="regular_text--advices">(Вы будете перенаправлены на предыдущую страницу через 3 секунды)</h3>' .
            '</div>'.
        '</MAIN>';
    header('refresh: 3, url=../../index.php');
    die;
}

$user = $stmt->fetch(PDO::FETCH_ASSOC);

if (password_verify($_POST['user_password'], $user['User_Password'])) {
    
    if (password_needs_rehash($user['User_Password'], PASSWORD_ARGON2ID)) {
        $newHash = password_hash($_POST['user_password'], PASSWORD_ARGON2ID);
        $stmt = pdo()->prepare(
            'UPDATE 
                `users` 
            SET 
                `User_Password` = :user_password 
            WHERE 
                `User_Username` = :user_login');
        $stmt->execute([
            'user_login' => $_POST['user_login'],
            'user_password' => $newHash,
        ]);
    }
    $_SESSION['user_id'] = $user['User_ID'];
    $_SESSION['user_role'] = $user['User_Role'];

    header('Location: ../about_me.php');
    die;
}
else {
    echo '<link rel="stylesheet" href="../../css/main.css">';
    echo
        '<MAIN id="error_page--auth" class="error_page">' .
            '<div class="error_notice">
                        <h2 class="regular_text--titles">Пароль не верный</h2>' .
            '<h3 class="regular_text--advices">(Вы будете перенаправлены на предыдущую страницу через 3 секунды)</h3>' .
            '</div>' .
        '</MAIN>';
    header('refresh: 3, url=../../index.php');
    die;
}