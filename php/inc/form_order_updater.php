<?php
require_once __DIR__ . '/do_connect.php';

$user = null;

if (check_auth()) {

    $stmt = pdo()->prepare("SELECT * FROM `users` WHERE `User_ID` = :id");
    $stmt->execute(['id' => $_SESSION['user_id']]);
    $user = $stmt->fetch(PDO::FETCH_ASSOC);
}
?>

<?php if((isset($_SESSION['user_id'])&& isset($_POST['row_to_edit__order_id'])) 
            && ($_SESSION['user_role'] >= '1' && $_SESSION['user_role'] <= '3')) {

?>
    <!DOCTYPE html>
    <HTML>

    <HEAD>
        <link rel="icon" href="../../images/logotypes/favicon.ico" type="image/x-icon">
        <link rel="stylesheet" href="../../css/main.css">
        <meta charset="utf-8">
        <title>Изменение заказа</title>
    </HEAD>

    <BODY>
        <HEADER>
            <div class="logo_with_descrptn">
                <img id="logotype" src="../../images/logotypes/favicon.ico" />
                <p class="regular_text--titles"><span id="description">Tortotoro</span></p>
                <p class="regular_text">для сотрудников</p>
            </div>
            <div class="advice_in_header">
                <?php
                    if ($_SESSION['user_role'] == '1'){
                        echo '<p class="regular_text--advices">Администратору - не терять бдительность и исполнительность</p>';
                    }
                    else {
                        echo '<p class="regular_text--advices">Будьте внимательны при изменении полей!</p>';
                    }
                ?>
            </div>
        </HEADER>
        <MAIN>
            <div class="update_in">
                <form id="updater_form" action="tables_work/order_edit.php" method="post">
                    <label class="regular_text--titles">Изменить заказ <?php echo '№ "'.$_POST['row_to_edit__order_id'].'"';?></label>
                    <div class="register_form__password_and_login">
                    
                    <?php
                        echo '<input type="hidden" name="row_to_edit__order_id" value="'.$_POST['row_to_edit__order_id'].'">';
                        echo '<input type="hidden" name="row_to_edit__order_old_status" value="'.$_POST['row_to_edit__order_status'].'">';
                    ?>
                            <?php
                                echo 
                                    '<div class="order_status-input">
                                        <label for="order_status">Текущее состояние заказа</label>
                                        <input class="regular_text regular_text--inputs" readonly type="text" id="order_status"
                                            value="'. $_POST['row_to_edit__order_status'] .'" name="order_status" placeholder="Введите номер состояния">
                                        <p class="regular_text--advices">Просматривая состояния, помни:</p>
                                        <ol id="order_states">
                                            <li class="regular_text--advices">1 - Принят (Назначается по умолчанию);</li>
                                            <li class="regular_text--advices">2 - Готовится (Назначается в том случае, если был принят
                                                поваром);</li>
                                            <li class="regular_text--advices">3 - Готов (Назначается поваром по приготовлении).</li>
                                            <li class="regular_text--advices">4 - Оплачен (Назначается официантом после оплаты).</li>
                                            <li class="regular_text--advices">5 - Отменён (Назначается официантом в случае эксцесса).</li>
                                        </ol>';
                            ?>
                            <?php
                                if ($_SESSION['user_role'] == '1'){

                                    echo 
                                            '<label for="order_status__select">Изменение состояния заказа</label>
                                            <select class="regular_text regular_text--inputs" name="order_status__select">
                                                <option value="1">Принят</option>
                                                <option value="2">Готовится</option>
                                                <option value="3">Готов</option>
                                                <option value="4">Оплачен</option>
                                                <option value="5">Отменён</option>
                                            </select></div>
                                        <div class="order_isPaid-input">
                                            <label for="order_isPaid">Текущее состояние оплаты</label>
                                            <input class="regular_text regular_text--inputs" type="number" id="order_isPaid"
                                                readonly value="'. $_POST['row_to_edit__order_IsPaid'] .'" name="order_isPaid" placeholder="Введите номер состояния">
                                            <p class="regular_text--advices">Просматривая состояния, помни:</p>
                                            <ol id="order_isPaid">
                                                <li class="regular_text--advices">0 - Не оплачен;</li>
                                                <li class="regular_text--advices">1 - Оплачен ;</li>
                                            </ol>
                                            <label for="order_IsPaid__select">Изменение состояния оплаты</label>
                                            <select class="regular_text regular_text--inputs" name="order_IsPaid__select">
                                                <option value="0">Не оплачен</option>
                                                <option value="1">Оплачен</option>
                                            </select>
                                        </div>
                                        <div class="submit-and-advice">
                                                <button class="regular_text--titles sumbit">Изменить</button>
                                        </div>';
                                }
                                
                                elseif ($_SESSION['user_role'] == '2'){

                                    if ($_POST['row_to_edit__order_status'] == 'Готов'){

                                        echo
                                            '<label for="order_status__select">Изменение состояния заказа</label>
                                            <select class="regular_text regular_text--inputs" name="order_status__select">
                                                <option value="4">Оплачен</option>
                                                <option value="5">Отменён</option>
                                            </select>
                                        </div>
                                        <div class="order_isPaid-input">
                                            <label for="order_isPaid">Текущее состояние оплаты</label>
                                            <input class="regular_text regular_text--inputs" type="number" id="order_isPaid"
                                                readonly value="'. $_POST['row_to_edit__order_IsPaid'] .'" name="order_isPaid" placeholder="Введите номер состояния">
                                            <p class="regular_text--advices">Просматривая состояния, помни:</p>
                                            <ol id="order_isPaid">
                                                <li class="regular_text--advices">0 - Не оплачен;</li>
                                                <li class="regular_text--advices">1 - Оплачен ;</li>
                                            </ol>
                                            <label for="order_IsPaid__select">Изменение состояния оплаты</label>
                                            <select class="regular_text regular_text--inputs" name="order_IsPaid__select">
                                                <option value="0">Не оплачен</option>
                                                <option value="1">Оплачен</option>
                                            </select>
                                        </div>
                                        <div class="submit-and-advice">
                                                <button class="regular_text--titles sumbit">Изменить</button>
                                        </div>';
                                    }

                                    else {
                                        echo '<h2 class="regular_text--titles">Заказ не готов, вы не можете его изменить</h2>';
                                    }
                                }

                                elseif ($_SESSION['user_role'] == '3'){

                                    if($_POST['row_to_edit__order_status'] == 'Принят' || $_POST['row_to_edit__order_status'] == 'Готовится'){

                                        echo 
                                            '<label for="order_status__select">Изменение состояния заказа</label>
                                            <select class="regular_text regular_text--inputs" name="order_status__select">
                                                <option value="2">Готовится</option>
                                                <option value="3">Готов</option>
                                            </select>
                                        </div>
                                        <div class="submit-and-advice">
                                                <button class="regular_text--titles sumbit">Изменить</button>
                                        </div>';
                                    }

                                    else {
                                        echo '
                                        <h3 class="regular_text--titles">Вы можете редактировать только принятые или готовящиеся заказы</h3>
                                        <h2 class="regular_text--advices">Вы не можете изменять статус оплаты у заказов</h2>
                                        </div>';
                                    }
                                }

                                else {
                                    echo 'Что-то пошло не так!</div>';
                                }   
                            ?>
                </form>
            </div>
        </MAIN>
    </BODY>

    </HTML>
<?php }
    elseif (!isset($_SESSION['user_id']) || !isset($_POST['row_to_edit__order_id'])){
        echo '<link rel="stylesheet" href="../../css/main.css">';
        echo
            '<MAIN id="error_page--auth" class="error_page">' .
            '<div class="error_notice">
                            <h2 class="regular_text--titles">Не были переданы необходимые данные</h2>' .
            '<h3 class="regular_text--advices">(Вы будете перенаправлены на страницу для авторизации через 3 секунды)</h3>' .
            '</div>' .
            '</MAIN>';
        header('refresh: 3, url=../../index.php');
        die;
    }

    else {
        echo '<link rel="stylesheet" href="../../css/main.css">';
        echo
            '<MAIN id="error_page--auth" class="error_page">' .
            '<div class="error_notice">
                            <h2 class="regular_text--titles">У вас нет доступа к этой странице</h2>' .
            '<h3 class="regular_text--advices">(Вы будете перенаправлены на страницу для авторизации через 3 секунды)</h3>' .
            '</div>' .
            '</MAIN>';
        header('refresh: 3, url=../../index.php');
        die;
} ?>