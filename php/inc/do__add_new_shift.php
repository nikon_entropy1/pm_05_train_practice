<?php

require_once __DIR__.'/do_connect.php';

$stmt = pdo()->prepare("SELECT * FROM `users` WHERE `User_Username` = :user_login");
$stmt->execute(['user_login' => $_POST['user_login']]);
if ($stmt->rowCount() > 0) {
    header("Location: {$_SERVER['HTTP_REFERER']}");
    die;
}

if (isset($_SESSION['user_id']) && $_SESSION['user_role'] == '1'){

    $stmt = pdo()->prepare(
        "INSERT INTO `shifts`
            (`Shift_Date`, 
            `Shift_Start__Time`, 
            `Shift_End__Time`,
            `Shift_Status`) 
            VALUES 
            (:shift_date, 
            :shift_start_time, 
            :shift_end_time,
            :shift_status);
    ");

    $stmt->execute([
        'shift_date' => $_POST['shift_date'],
        'shift_start_time' => $_POST['shift_start_time'],
        'shift_end_time' => $_POST['shift_end_time'],
        'shift_status' => strip_tags($_POST['shift_status']),
    ]);

    header("Location: {$_SERVER['HTTP_REFERER']}");
}

else {
        echo '<link rel="stylesheet" href="../../css/main.css">';
        echo
                '<MAIN id="error_page--auth" class="error_page">' .
                '<div class="error_notice">
                                <h2 class="regular_text--titles">У вас нет доступа к этой странице</h2>' .
                '<h3 class="regular_text--advices">(Вы будете перенаправлены на страницу для авторизации через 3 секунды)</h3>' .
                '</div>' .
                '</MAIN>';
        header('refresh: 3, url=../../index.php');
        die;
}
