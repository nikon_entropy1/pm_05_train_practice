<?php 
    require_once __DIR__.'/inc/do_connect.php';
?>

<?php if(isset($_SESSION['user_id'])) {?>
<?php
    $stmt = pdo()->prepare(
        "SELECT
            Employee_Firstname,
            Role_Name
        FROM
            personell
        JOIN
            users
        ON
            User_ID = User_ID__FK
        JOIN
            roles
        ON
            Role_ID = User_Role
        WHERE `Employee_ID` = :user_id");
    $stmt->execute(['user_id' => $_SESSION['user_id']]);                
    $user_role = $stmt->fetch(PDO::FETCH_ASSOC);    
?>
<!DOCTYPE html>
<HTML>

<HEAD>
    <link rel="icon" href="../images/logotypes/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../css/main.css">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Пароль</title>
</HEAD>

<BODY>
    <HEADER>
        <div class="logo_with_descrptn">
            <img alt="Логотип" id="logotype" src="../images/logotypes/favicon.ico" />
            <p class="regular_text--titles"><span id="description">Tortotoro</span> для сотрудников</p>
        </div>
        <div id="#page_now" class="page_pointer">
            <a href="#page_now">
                <p class="regular_text">
                    Tortotoro для сотрудников » <span class="page_pointer--state">Пароль</span>
                </p>
            </a>
        </div>
    </HEADER>
    <MAIN id="pass4change">
        <div class="password_change">
            <div class="password_change__title">
                <h3 class="regular_text">Смена пароля</h3>
            </div>
            <form id="password_changer" action="inc/password_changer.php" method="post" class="password_change__to-do">
                <div class="password-repeat">
                    <div class="old_pass">
                        <label for="old_pass"class="regular_text">Текущий пароль</label>
                        <input required class="regular_button" type="password" name="old_pass">
                    </div>
                    <div class="new_pass">
                        <label for="new_pass" class="regular_text">Новый пароль</label>
                        <input required class="regular_button" type="password" id="new_pass" name="new_pass">
                    </div>
                    <div class="new_pass__check">
                        <label for="new_pass_check"class="regular_text">Подтвердите новый пароль</label>
                        <input required class="regular_button" type="password" id="new_pass_check" name="new_pass_check">
                    </div>
                </div>
                <div class="buttons_of_submit">
                    <input type="submit" id="" class="regular_text regular_button" value="Сменить пароль"></input>
                    <input type="reset" id="password_clear" class="regular_text regular_button" value="Отменить"></input>
                </div>
            </form>
        </div>
        <div class="right_menu_navigation">
            <nav id="right_menu">
                <li class="right_menu__element regular_text"><a href="about_me.php">Обо мне</a></li>
                <li class="right_menu__element regular_text"><a href="pass4change.php">Пароль</a></li>
                <li class="right_menu__element regular_text"><a href="actual_orders.php">Просмотреть заказы текущей смены</a></li>
                <?php
                    if ($_SESSION['user_role'] == '1'){
                        echo '
                            <li class="right_menu__element regular_text"><a href="edit__shifts.php">Редактировать смены</a></li>
                            <li class="right_menu__element regular_text"><a href="edit__employees.php">Редактировать сотрудников</a></li>
                        ';
                    }
                ?>
                <li class="right_menu__element to_exit_from_LK">
                    <form id="deauth" method="post" action="inc/do_logOUT.php">
                        <button id="to_exit-button" class="regular_text">Выйти</button>
                    </form>
                </li>
            </nav>
        </div>
    </MAIN>
    <FOOTER>
        <div class="hello_to_user_and_his_role">
            <?php
                echo 
                    '<p class="regular_text--advices">
                        Здравствуйте, '.stripslashes($user_role["Employee_Firstname"]).'! Вы зашли как <span class="footer_highlight--user_role">'.stripslashes($user_role["Role_Name"]).'</span>
                    </p>';
            ?>
        </div>
        <div class="button_to_deauth">
            <form id="deauth" method="post" action="inc/do_logOUT.php">
                <button id="to_exit-button" class="regular_text">Выйти</button>
            </form>
        </div>
        <a id="page_up_down_button" href="#">Наверх</a>
    </FOOTER>
</BODY>

</HTML>
<?php } else {
    echo '<link rel="stylesheet" href="../css/main.css">';
    echo
        '<MAIN id="error_page--auth" class="error_page">' .
            '<div class="error_notice">
                        <h2 class="regular_text--titles">Вы не зарегистрированы</h2>' .
            '<h3 class="regular_text--advices">(Вы будете перенаправлены на страницу для авторизации через 3 секунды)</h3>' .
            '</div>' .
        '</MAIN>';
    header('refresh: 3, url=../index.php');
    die;
}?>