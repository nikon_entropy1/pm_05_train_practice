<?php 
    require_once __DIR__.'/inc/do_connect.php';
?>

<?php if(isset($_SESSION['user_id']) && $_SESSION['user_role'] == '1') {?>
<!DOCTYPE html>
<HTML>

<HEAD>
    <link rel="icon" href="../images/logotypes/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../css/main.css">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Смены</title>
</HEAD>

<BODY>
    <HEADER>
        <div class="logo_with_descrptn">
            <img alt="Логотип" id="logotype" src="../images/logotypes/favicon.ico" />
            <p class="regular_text--titles"><span id="description">Tortotoro</span> для сотрудников</p>
        </div>
        <div id="#page_now" class="page_pointer">
            <a href="#page_now">
                <p class="regular_text">
                    Tortotoro для сотрудников » <span class="page_pointer--state">Редактирование смен</span>
                </p>
            </a>
        </div>
    </HEADER>
    <MAIN id="edit__shifts">
        <div class="table_exists_shifts">
            <table class="table_of_shifts">
                <thead>
                    <legend class="table__title table_of_shifts__title regular_text--titles">Таблица смен
                        <form method="post" action="inc/form_shift_register.php">
                            <button class="regular_button" type="submit" name="submit"><img alt="Добавить" src="../images/icons/tables_add.svg"/></button>
                        </form>
                    </legend>
                    <th class="regular_text--table_titles">Номер смены</th>
                    <th class="regular_text--table_titles">Дата смены</th>
                    <th class="regular_text--table_titles">Дата начала смены</th>
                    <th class="regular_text--table_titles">Дата окончания смены</th>
                    <th class="regular_text--table_titles">Статус смены</th>
                    <th class="regular_text--table_titles">Действие</th>
                </thead>
                <tbody>
                    <?php
                        require_once 'inc/do_connect.php';
                        $stmt = pdo()->prepare("CALL shifts_list()");
                        $stmt->execute();
                    ?>
                    <?php
                        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                            echo
                            '<tr>' .
                                '<td>' .
                                    stripslashes($row["Shift_ID"]) .
                                '</td>' .
                                '<td>' .
                                    stripslashes($row["Shift_Date"]) .
                                '</td>' .
                                '<td>' .
                                    stripslashes($row["Shift_Start__Time"]) .
                                '</td>' .
                                '<td>' .
                                    stripslashes($row["Shift_End__Time"]) .
                                '</td>' .
                                '<td>' .
                                    stripslashes($row["Shift_State__Desciption"]) .
                                '</td>' .
                                '<td>' .
                                    '<div id="editable_td">
                                        <form method="post" action="inc/form_shift_updater.php">
                                            <input type="hidden" name="row_to_edit__shift_id" value="' . stripslashes($row["Shift_ID"]) . '">
                                            <input type="hidden" name="row_to_edit__shift_date" value="' . stripslashes($row["Shift_Date"]) . '">
                                            <input type="hidden" name="row_to_edit__shift_start_time" value="' . stripslashes($row["Shift_Start__Time"]) . '">
                                            <input type="hidden" name="row_to_edit__shift_end_time" value="' . stripslashes($row["Shift_End__Time"]) . '">
                                            <input type="hidden" name="row_to_edit__shift_state_description" value="' . stripslashes($row["Shift_State__Desciption"]) . '">
                                            <button type="submit" name=""><img alt="Отредактировать" src="../images/icons/tables_edit.svg"/></button>
                                        </form>
                                        <form method="post" action="inc/tables_work/shift_delete.php">
                                            <input type="hidden" name="row_to_delete__shift_id" value="' . stripslashes($row["Shift_ID"]) . '">
                                            <button type="submit" name=""><img alt="Удалить" src="../images/icons/tables_delete.svg"/></button>
                                        </form>
                                    </div>
                                </td>' .
                            '</tr>';
                        }
                    ?>
                </tbody>
            </table>
            </div>
            <div class="right_menu_navigation">
                <nav id="right_menu">
                    <li class="right_menu__element regular_text"><a href="about_me.php">Обо мне</a></li>
                    <li class="right_menu__element regular_text"><a href="pass4change.php">Пароль</a></li>
                    <li class="right_menu__element regular_text"><a href="actual_orders.php">Просмотреть заказы текущей смены</a></li>
                    <li class="right_menu__element regular_text"><a href="edit__shifts.php">Редактировать смены</a></li>
                    <li class="right_menu__element regular_text"><a href="edit__employees.php">Редактировать сотрудников</a></li>
                    <li class="right_menu__element to_exit_from_LK">
                        <form id="deauth" method="post" action="inc/do_logOUT.php">
                            <button id="to_exit-button" class="regular_text">Выйти</button>
                        </form>
                    </li>
                </nav>
            </div>
        </MAIN>
        <FOOTER>
            <div class="hello_to_user_and_his_role">
                <?php
                    $stmt = pdo()->prepare(
                        "SELECT
                            Employee_Firstname,
                            Role_Name
                        FROM
                            personell
                        JOIN
                            users
                        ON
                            User_ID = User_ID__FK
                        JOIN
                            roles
                        ON
                            Role_ID = User_Role
                        WHERE `Employee_ID` = :user_id");
                    $stmt->execute(['user_id' => $_SESSION['user_id']]);                
                    $user_role = $stmt->fetch(PDO::FETCH_ASSOC);
                    
                    echo 
                    '<p class="regular_text--advices">
                        Здравствуйте, '.stripslashes($user_role["Employee_Firstname"]).'! Вы зашли как <span class="footer_highlight--user_role">'.stripslashes($user_role["Role_Name"]).'</span>
                    </p>';
                ?>
            </div>
            <div class="button_to_deauth">
                <form id="deauth" method="post" action="inc/do_logOUT.php">
                    <button id="to_exit-button" class="regular_text">Выйти</button>
                </form>
            </div>
            <a id="page_up_down_button" href="#">Наверх</a>
        </FOOTER>
    </BODY>

    </HTML>
<?php
    } else {
    echo '<link rel="stylesheet" href="../css/main.css">';
    echo
        '<MAIN id="error_page--auth" class="error_page">' .
            '<div class="error_notice">
                        <h2 class="regular_text--titles">У вас нет доступа к этой странице</h2>' .
            '<h3 class="regular_text--advices">(Вы будете перенаправлены на страницу для авторизации через 3 секунды)</h3>' .
            '</div>' .
        '</MAIN>';
    header('refresh: 3, url=../index.php');
    die;
}?>