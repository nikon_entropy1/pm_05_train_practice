<?php
    require 'inc/do_connect.php';
?>

<?php if(isset($_SESSION['user_id'])) {?>
<?php 
    $stmt = pdo()->prepare(
        "SELECT
            Employee_Firstname,
            Employee_Lastname,
            Employee_Patronimyc,
            Employee_Email,
            Employee_Telephone,
            Employee_Birthday,
            Role_Name,
            Role_ID,
            Gender_Description,
            Avatar_Src
        FROM
            personell
        JOIN
            users
        ON
            Employee_ID = User_ID
        JOIN
            roles
        ON
            User_Role = Role_ID
        JOIN
            genders
        ON
            Employee_Gender = Gender_ID
        JOIN 
            `user_photo__avatars-samples`
        ON
            User_Avatar = Avatar_ID
        WHERE `Employee_ID` = :user_id");

    $stmt->execute(
        ['user_id' => $_SESSION['user_id']]);
    $row = $stmt->fetch(PDO::FETCH_ASSOC); 
?>

<!DOCTYPE html>
<HTML>

<HEAD>
    <link rel="icon" href="../images/logotypes/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../css/main.css">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Обо мне</title>
</HEAD>

<BODY>
    <HEADER>
        <div class="logo_with_descrptn">
            <img alt="Логотип" id="logotype" src="../images/logotypes/favicon.ico" />
            <p class="regular_text--titles"><span id="description">Tortotoro</span> для сотрудников</p>
        </div>
        <div id="#page_now" class="page_pointer">
            <a href="#page_now">
                <p class="regular_text">
                    Tortotoro для сотрудников » <span class="page_pointer--state">Обо мне</span>
                </p>
            </a>
        </div>
    </HEADER>
    <MAIN id="about_me">
        <div class="user_info">
            <div class="user_photo">
                <?php echo '<img alt="Фотография пользователя" src="' . stripslashes($row["Avatar_Src"]) . '"/>';?>
            </div>
            <div class="user_about-grid">
                <div class="user_about-grid__element">
                    <label class="regular_text">Роль в ИС</label>
                    <?php echo 
                        '<input type="text" readonly name="user_role" 
                        placeholder="'.stripslashes($row["Role_Name"]).'"/>
                </div>'; 
                    ?>
                <div class="user_about-grid__element">
                    <label class="regular_text">Имя сотрудника</label>
                    <?php echo 
                        '<input type="text" readonly name="user_name" 
                        placeholder="'.stripslashes($row["Employee_Firstname"]).'"/>
                </div>'; 
                    ?>
                <div class="user_about-grid__element">
                    <label class="regular_text">Фамилия сотрудника</label>
                    <?php echo 
                        '<input type="text" readonly name="user_surname" 
                        placeholder="'.stripslashes($row["Employee_Lastname"]).'"/>
                </div>'; 
                    ?>
                <div class="user_about-grid__element">
                    <label class="regular_text">Отчество сотрудника</label>
                    <?php echo 
                        '<input type="text" readonly name="user_patronimyc" 
                        placeholder="'.stripslashes($row["Employee_Patronimyc"]).'"/>
                </div>'; 
                    ?>
                <div class="user_about-grid__element">
                    <label class="regular_text">Email сотрудника</label>
                    <?php echo 
                        '<input type="text" readonly name="user_email" 
                        placeholder="'.stripslashes($row["Employee_Email"]).'"/>
                </div>'; 
                    ?>
                <div class="user_about-grid__element">
                    <label class="regular_text">Номер телефона сотрудника</label>
                    <?php echo 
                        '<input type="text" readonly name="user_telephone" 
                        placeholder="'.stripslashes($row["Employee_Telephone"]).'"/>
                </div>'; 
                    ?>
                <div class="user_about-grid__element">
                    <label class="regular_text">Дата рождения сотрудника</label>
                    <?php echo 
                        '<input type="text" readonly name="user_birthday" 
                        placeholder="'.stripslashes($row["Employee_Birthday"]).'"/>
                </div>'; 
                    ?>
                <div class="user_about-grid__element">
                    <label class="regular_text">Пол сотрудника</label>
                    <?php echo 
                        '<input type="text" readonly name="user_gender" 
                        placeholder="'.stripslashes($row["Gender_Description"]).'"/>
                </div>'; 
                    ?>
            </div>
        </div>
        <div class="right_menu_navigation">
            <nav id="right_menu">
                <li class="right_menu__element regular_text"><a href="about_me.php">Обо мне</a></li>
                <li class="right_menu__element regular_text"><a href="pass4change.php">Пароль</a></li>
                <li class="right_menu__element regular_text"><a href="actual_orders.php">Просмотреть заказы текущей смены</a></li>
                <?php
                    if ($_SESSION["user_role"] == '1'){
                        echo '
                            <li class="right_menu__element regular_text"><a href="edit__shifts.php">Редактировать смены</a></li>
                            <li class="right_menu__element regular_text"><a href="edit__employees.php">Редактировать сотрудников</a></li>
                        ';
                    }
                ?>
                <li class="right_menu__element to_exit_from_LK">
                    <form id="deauth" method="post" action="inc/do_logOUT.php">
                        <button id="to_exit-button" class="regular_text">Выйти</button>
                    </form>
                </li>
            </nav>
        </div>
    </MAIN>
    <FOOTER>
        <div class="hello_to_user_and_his_role">
            <?php
                echo 
                '<p class="regular_text--advices">
                    Здравствуйте, '.stripslashes($row["Employee_Firstname"]).'! Вы зашли как <span class="footer_highlight--user_role">'.stripslashes($row["Role_Name"]).'</span>
                </p>';
            ?>
        </div>
        <div class="button_to_deauth">
            <form id="deauth" method="post" action="inc/do_logOUT.php">
                <button id="to_exit-button" class="regular_text">Выйти</button>
            </form>
        </div>
        <a id="page_up_down_button" href="#">Наверх</a>
    </FOOTER>
</BODY>

</HTML>
<?php } else {
    echo '<link rel="stylesheet" href="../css/main.css">';
    echo
        '<MAIN id="error_page--auth" class="error_page">' .
            '<div class="error_notice">
                        <h2 class="regular_text--titles">Вы не зарегистрированы</h2>' .
            '<h3 class="regular_text--advices">(Вы будете перенаправлены на страницу для авторизации через 3 секунды)</h3>' .
            '</div>' .
        '</MAIN>';
    header('refresh: 3, url=../index.php');
    die;
}?>